## Introduction

Daknet Platform is a e-commerce framework built for [Laravel](https://laravel.com). This provides a way to lessen the time in building online stores.

### Requirements

The minimum Laravel version required is v5.8.

### Installation

To get started with Platform, use Composer to install Platform into your Laravel project:

```bash
composer require daknet/platform
```

After installing Platform, publish it's config file and assets (JS/CSS and images):

```bash
php artisan platform:install
```

